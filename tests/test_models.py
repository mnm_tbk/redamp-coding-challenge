import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from models import IP, URL
from settings import db_connection_str


class TestIpModel:
    testdata_IP = [
        "800.800.800.800",
        "test",
        "FFFF:FFFF:GGGG:",
    ]

    @pytest.mark.parametrize("addr", testdata_IP)
    def test_ip_validation(self, addr):
        with pytest.raises(AssertionError) as e_info:
            record = IP(source='test', address=addr)

    testdata_source = [
        "",
        None
    ]

    @pytest.mark.parametrize("source", testdata_source)
    def test_empty_source(self, source):
        with pytest.raises(AssertionError) as e_info:
            record = IP(source=source, address="asd")

    def test_succ_save(self, sa_db):
        record = IP(source="test", address='127.0.0.5')

        sa_db.add(record)
        sa_db.commit()

        from_db = sa_db.query(IP).filter(IP.address == record.address)
        assert from_db


class TestURLModel:
    testdata_URL = [
        "fd://178.62.47.209/banks/ATB/last.html",
        "google.",
        "123",
    ]

    @pytest.mark.parametrize("path", testdata_URL)
    def test_url_validation(self, path):
        with pytest.raises(AssertionError) as e_info:
            record = URL(source='test', full_path=path)

    testdata_URL = [
        "http://178.62.47.209/banks/ATB/last.html",
        "http://foo.com/blah_(wikipedia)#cite-1",
        "http://userid:password@example.com",
        "https://www.example.com/foo/?bar=baz&inga=42&quux"
    ]

    @pytest.mark.parametrize("path", testdata_URL)
    def test_valid_path(self, path, sa_db):
        record = URL(source="test", full_path=path)

        sa_db.add(record)
        sa_db.commit()

        from_db = sa_db.query(URL).filter(URL.full_path == record.full_path)
        assert from_db
