import pytest

from importers.IPImporter import IpImporter, ExtendedIpImporter
from importers.URLImporter import URLImporter


class TestImporters:
    TEST_DATA = [
        ("127.0.0.1\n127.0.0.2", IpImporter()),
        ("http://vikendvkomatu.com\nhttps://vikendvkomatu.com", URLImporter()),
        ("""42.224.76.110#4#2#Malicious Host#CN##34.6836013794,113.532501221#3\n36.238.145.95#1#3#Malicious Host#TW##23.5,121.0#3""",
         ExtendedIpImporter())
    ]

    @pytest.mark.parametrize("i,importer", TEST_DATA)
    def test_importer_extract(self, requests_mock, i, importer):

        requests_mock.get(importer.SOURCE, text=i)

        for i in importer.extract():
            assert i

    @pytest.mark.parametrize("i,importer", TEST_DATA)
    def test_importer_transform(self, requests_mock, i, importer):
        resp_parsed = i.split("\n")
        requests_mock.get(importer.SOURCE, text=i)

        for i, data in enumerate(importer.extract()):
            rec = importer.transform(data)
            assert rec

    @pytest.mark.parametrize("i,importer", TEST_DATA)
    def test_imoprter_run(self, requests_mock, sa_db, i, importer):
        requests_mock.get(importer.SOURCE, text=i)

        importer.run()

        assert sa_db.query(importer.MODEL).count() == 2
