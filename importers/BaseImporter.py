import logging
from abc import ABC, abstractmethod
from pprint import pprint
from typing import Iterator, Counter, List, Tuple, Optional
from collections import Counter as Cnt

import requests
from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm.session import Session

from models import IOC
from settings import db_connection_str


class BaseImporter(ABC):
    """
    Abstract interface for importers.

    """
    SOURCE = None
    COUNTER: Counter = None
    MODEL: IOC = None

    def __init__(self, session: Session = None, very_verbose=False):
        self.session = session or Session(bind=create_engine(db_connection_str))
        self.logger = logging.getLogger('app')
        self.very_verbose = very_verbose

    def extract(self) -> Iterator[str]:
        data = requests.get(self.SOURCE).text
        data_lines_list = data.split("\n")
        for line in data_lines_list:
            yield line

    @abstractmethod
    def transform(self, raw_data: str) -> Optional[IOC]:
        pass

    def load(self, obj: IOC) -> bool:
        if obj:
            try:
                self.session.add(obj)
                self.session.commit()
                if self.very_verbose:
                    self.logger.info(f"Saved {obj!r}")
                    self.COUNTER["saved"] += 1
                return True
            except IntegrityError:
                self.session.rollback()
                if self.very_verbose:
                    self.COUNTER["not saved"] += 1
                    self.logger.info(f"Integrity error during loading of IOC record {obj!r}")
                return False

    def run(self):
        """
        template method to run parts of the ETL in the right order
        """
        for i, data in enumerate(self.extract()):
            if i % 100 == 0 and self.very_verbose:
                self.logger.info(self.COUNTER)
                self.logger.info(f"after parsing {i} records")
            try:
                self.load(self.transform(data))
            except IntegrityError:
                self.session.rollback()
                self.logger.info(f"Integrity error during loading of IOC record {data!r}")
        self.logger.info(f"Importing of {self.__class__.__name__} is done")
        self.logger.info(pprint(self.COUNTER))
        self.COUNTER = Cnt()
