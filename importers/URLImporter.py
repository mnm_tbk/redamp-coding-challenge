from collections import Counter
from typing import Iterator

import requests
from sqlalchemy.exc import IntegrityError

from importers.BaseImporter import BaseImporter
from models import IOC, URL


class URLImporter(BaseImporter):
    SOURCE = "https://openphish.com/feed.txt"
    COUNTER = Counter()
    MODEL = URL

    def transform(self, raw_data: str) -> URL:
        try:
            return URL(source=self.SOURCE, full_path=raw_data)
        except AssertionError:
            raise IntegrityError("URL not valid", raw_data, None)
