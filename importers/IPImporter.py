from collections import Counter
from collections import Counter
from typing import Tuple, Optional

from sqlalchemy.exc import IntegrityError

from importers.BaseImporter import BaseImporter
from models import IP, ExtendedIP


class IpImporter(BaseImporter):
    SOURCE = 'https://www.badips.com/get/list/any/2'
    COUNTER = Counter()
    MODEL = IP

    def transform(self, raw_data: str) -> IP:
        try:
            return IP(address=raw_data.strip(), source=self.SOURCE)
        except AssertionError:
            self.logger.error(f"Couldn't parse IP instance {raw_data}, ip addr not in the right format")
            raise IntegrityError(f"Couldn't parse IP instance ", raw_data, orig=None)


class ExtendedIpImporter(BaseImporter):
    SOURCE = 'http://reputation.alienvault.com/reputation.data'
    COUNTER = Counter()
    MODEL = ExtendedIP

    def transform(self, raw_data: str) -> Optional[ExtendedIP]:
        if not raw_data:
            return None

        line_values = raw_data.strip().split("#")
        if line_values:
            try:
                record = ExtendedIP(address=line_values[0],
                                    source=self.SOURCE,
                                    risk=int(line_values[1]),
                                    reliability=int(line_values[2]),
                                    reason=line_values[3],
                                    country_code=line_values[4],
                                    city=line_values[5],
                                    )
                if len(line_values) > 5:
                    lat_lon = line_values[6].split(",")
                    record.lat = lat_lon[0]
                    record.lon = lat_lon[1]

                return record
            except AssertionError:
                self.logger.error(f"Couldn't parse IP instance {raw_data}, ip addr not in the right format")
                raise IntegrityError(f"Couldn't parse IP instance ", raw_data, orig=None)
            except IndexError as e:
                self.logger.error(e.args)
                self.logger.error(f"Couldn't parse IP instance {raw_data}, some attributes missing")
