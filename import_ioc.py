from typing import Tuple

import click

from importers.IPImporter import IpImporter, ExtendedIpImporter
from importers.URLImporter import URLImporter

IMPORTERS_DICT = {"ip": IpImporter,
                  "extended_ip": ExtendedIpImporter,
                  "url": URLImporter
                  }


@click.command()
@click.option('--verbose', is_flag=True)
@click.option('--importers', '-i',
              type=click.Choice(['ip', 'extended_ip', 'url'], case_sensitive=False),
              multiple=True,
              default=["ip"])
def import_ioc(verbose, importers: Tuple[str]):
    """CLI interface for importing IOC from 3 sources into a relational database"""
    print(importers)
    for _ in importers:
        try:
            click.echo(f"executing {_}...")
            IMPORTERS_DICT[_](very_verbose=True if verbose else False).run()
        except KeyError:
            raise ValueError(f"Unknown importer: {_}, possible choices are ip, extended_ip, url")


if __name__ == '__main__':
    import_ioc()
