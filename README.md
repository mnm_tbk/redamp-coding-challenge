# redamp-coding-challenge

Hi, this is my coding assignment for Redamp company. 

## Requirements

This project uses [Poetry](https://python-poetry.org) as a package manager.

The "production" and "testing" setup is managed by docker-compose and docker.
So you will probably want to install these services, in order to have a pain free experience.

## Summary
I had a hard time deciding what tools and libraries to use. 
The assignment was asking for a command-line tool for downloading IOCs from various sources.

_Django_ would be a perfect fit for the job:
1. it offers a clear framework for writing command-line tools: Django management commands
1. it provides us with the ORM, Lazy Loading, Model interface, migrations, and other useful tools
1. admin interface could be used for viewing contents of the database and interact with it

Instead, I opted for a more minimalistic solution:
1. _SQLAlchemy_ as an ORM library
1. _click_ as a command-line library
1. _pgAdmin_ as a DB administration solution
1. _pytest_ with a custom fixture for db creation/reuse

The reasoning behind this decision is simple: my task requirements were a command-line script, not a web app.
Perhaps the script could be deployed to a rather simple device where space on the disk is sacred. 

### Model class diagram
The class digram of the models is pretty simple:

![Class Diagram](class-diagram.png)

Four models:
- IOC: abstract model with `source` and `id` fields
- IP: model for storing IPv4 and IPv6 addresses
- Extended IP: model for storing extra metadata about IP records like location and risk
- URL: model for storing malicious URLs

## Contents 

```
.
├── Dockerfile
├── README.md
├── conftest.py     <- pytest config file 
├── docker-compose.testing.yml  <- docker-compose testing config
├── docker-compose.yml          <- docker-compose run config
├── docker-run-tests.sh
├── docker-run.sh
├── import_ioc.py   <- main script
├── importers       <- package with importer classes
│   ├── BaseImporter.py
│   ├── IPImporter.py
│   ├── URLImporter.py
│   └── __init__.py
├── migrate.py      <- script for building db
├── models.py       <- model layer
├── poetry.lock     <- poetry file
├── pyproject.toml  <- poetry file
├── pytest.ini      <- pytest config file 
├── requirements.txt <- requirements for docker
├── settings.py     <- settings 
└── tests
    ├── __init__.py
    ├── test_importers.py
    └── test_models.py
```

## Run the command
1. `docker-compose up` will spin up all of the containers and run `python import_ioc.py -i ip -i extended_ip -i url`
1. navigate to http://0.0.0.0:8080/ for access to pgAdmin; all of the credentials are located in ***docker-compose.yml***.
You have to add a new connection, details of the database connection are located in ***docker-compose.yml***.

## Testing
1. `docker-compose -f docker-compose.testing.yml up` will spin up a database and run `pytest`
2. you can view the test report along with the coverage report by navigating to http://0.0.0.0:8000/test_report.html and http://0.0.0.0:8000/

## Further refinements
Well, the speed of the importing script could be improved by a great deal by spinning up new processes for each source to be imported.
We could go even further and parallelize each task on the data layer: splitting the response from the server into subsets and parsing them individually.  

Async coroutines could help with saving the data into the database; these are the only extensive I/O operations happening here.  

It would be possible to implement caching by creating a hash from the server response and comparing the hashes between consecutive imports of the same service.

The project does not offer a way to create and run migrations, this limits us to drop all tables on schema changes.
This problem could be solved by integrating _alembic_ package 