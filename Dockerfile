FROM python:3.7-buster

RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/
COPY docker-run.sh /code/
RUN pip install -r requirements.txt
COPY . /code/
#RUN ./migrate.py
