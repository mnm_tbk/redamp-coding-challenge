import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import Session

from settings import db_connection_str


@pytest.fixture(scope="function")
def sa_db():
    """
    provides db access in tests (deleting all data between each test)
    """
    import contextlib
    from sqlalchemy import MetaData

    meta = MetaData()
    engine = create_engine(db_connection_str)

    with contextlib.closing(engine.connect()) as con:
        trans = con.begin()
        for table in reversed(meta.sorted_tables):
            con.execute(table.delete())
        trans.commit()

    session = Session(bind=engine)
    return session


collect_ignore_glob = [
    '*_ignore.py',
    '*wsgi/*',
    '*venv*/*',
    '*bin/*',
    '*lib/*',
    '*lib64/*',
    '*local/*',
]
