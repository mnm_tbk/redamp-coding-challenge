import re
import typing

import sqlalchemy as sa
from sqlalchemy import Column, Integer, Text, String, Boolean, Numeric, ForeignKey, Float
from sqlalchemy.ext.declarative import as_declarative, declared_attr, declarative_base
from sqlalchemy.orm import validates
from sqlalchemy_utils import IPAddressType, URLType


@as_declarative()
class IOC:
    id = Column(Integer, primary_key=True, autoincrement=True)
    source = Column(URLType, nullable=False)

    def __repr__(self) -> str:
        return self._repr(id=self.id)

    def _repr(self, **fields) -> str:
        """
        Helper for __repr__
        """
        field_strings = []
        at_least_one_attached_attribute = False
        for key, field in fields.items():
            try:
                field_strings.append(f'{key}={field!r}')
            except sa.orm.exc.DetachedInstanceError:
                field_strings.append(f'{key}=DetachedInstanceError')
            else:
                at_least_one_attached_attribute = True
        if at_least_one_attached_attribute:
            return f"<{self.__class__.__name__}({','.join(field_strings)})>"
        return f"<{self.__class__.__name__} {id(self)}>"

    @validates("source")
    def validate_source(self, key, value):
        assert value, "source cannot be empty"
        return value


class URL(IOC):
    __tablename__ = 'url'
    full_path = Column(URLType, nullable=False)

    @validates("full_path")
    def validate_full_path(self, key, value):
        """
        regex source https://stackoverflow.com/questions/3809401/what-is-a-good-regular-expression-to-match-a-url
        """
        regexp = r'https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)'
        assert re.match(regexp, value), "address is not valid URL"
        return value


class IP(IOC):
    __tablename__ = 'ip'
    address = Column(IPAddressType, unique=True, nullable=False)

    @validates('address')
    def validate_address(self, key, value):
        """
        regex source https://www.regexpal.com/?fam=104038
        """
        regexp = r'((^\s*((([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]))\s*$)|(^\s*((([0-9A-Fa-f]{1,4}:){7}([0-9A-Fa-f]{1,4}|:))|(([0-9A-Fa-f]{1,4}:){6}(:[0-9A-Fa-f]{1,4}|((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){5}(((:[0-9A-Fa-f]{1,4}){1,2})|:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3})|:))|(([0-9A-Fa-f]{1,4}:){4}(((:[0-9A-Fa-f]{1,4}){1,3})|((:[0-9A-Fa-f]{1,4})?:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){3}(((:[0-9A-Fa-f]{1,4}){1,4})|((:[0-9A-Fa-f]{1,4}){0,2}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){2}(((:[0-9A-Fa-f]{1,4}){1,5})|((:[0-9A-Fa-f]{1,4}){0,3}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(([0-9A-Fa-f]{1,4}:){1}(((:[0-9A-Fa-f]{1,4}){1,6})|((:[0-9A-Fa-f]{1,4}){0,4}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:))|(:(((:[0-9A-Fa-f]{1,4}){1,7})|((:[0-9A-Fa-f]{1,4}){0,5}:((25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)(\.(25[0-5]|2[0-4]\d|1\d\d|[1-9]?\d)){3}))|:)))(%.+)?\s*$))'  # TODO ipv6
        assert re.match(regexp, value), "not IPv4 nor IPv6"
        return value

    def __repr__(self):
        return self._repr(address=self.address,
                          source=self.source)


class ExtendedIP(IP):
    __tablename__ = 'extended_ip'
    __mapper_args__ = {'polymorphic_identity': 'extended_ip'}
    id = Column(Integer, ForeignKey('ip.id'), primary_key=True)

    risk = Column(Integer)
    reliability = Column(Integer)

    reason = Column(String)

    country_code = Column(String)
    city = Column(String)

    lat = Column(Float(precision=20))
    lon = Column(Float(precision=20))

    def __repr__(self):
        return self._repr(address=self.address,
                          source=self.source,
                          country_code=self.country_code,
                          city=self.city)
