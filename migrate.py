from sqlalchemy import create_engine, MetaData
import sqlalchemy as sqla
from settings import db_connection_str


from models import IOC

engine = create_engine(db_connection_str)

to_delete = ['ip', 'extended_ip', 'url']


def create_db():
    """
    drops all tables and builds new schema
    """
    for table in engine.table_names():
        # Delete only the tables in the delete list
        if table in to_delete:
            sql = sqla.text("DROP TABLE IF EXISTS {} CASCADE".format(table))
            engine.execute(sql)

    IOC.metadata.create_all(engine)


if __name__ == "__main__":
    create_db()