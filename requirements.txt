atomicwrites==1.4.0; sys_platform == "win32"
attrs==19.3.0
certifi==2020.6.20
chardet==3.0.4
click==7.1.2
colorama==0.4.3; sys_platform == "win32"
coverage==4.5.4
idna==2.10
importlib-metadata==1.7.0; python_version < "3.8"
more-itertools==8.4.0
packaging==20.4
pluggy==0.13.1
psycopg2==2.8.5
py==1.9.0
pyparsing==2.4.7
pytest==5.4.3
pytest-cov==2.10.0
pytest-cover==3.0.0
pytest-coverage==0.0
pytest-html==2.1.1
pytest-metadata==1.10.0
pytest-timeout==1.4.2
requests==2.24.0
requests-mock==1.8.0
six==1.15.0
sqlalchemy==1.3.18
sqlalchemy-utils==0.36.8
urllib3==1.25.9
wcwidth==0.2.5
zipp==3.1.0; python_version < "3.8"
